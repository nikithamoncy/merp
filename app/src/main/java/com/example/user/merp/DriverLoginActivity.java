package com.example.user.merp;

import android.content.Context;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;


public class DriverLoginActivity extends ActionBarActivity {

    Button btn0_d;
    Button btn1_d;
    Button btn2_d;
    Button btn3_d;
    Button btn4_d;
    Button btn5_d;
    Button btn6_d;
    Button btn7_d;
    Button btn8_d;
    Button btn9_d;
    Button btnC_d;

    Button driver_login;

    EditText driver_password;
    TextView driver_name;
    ImageView driver_img;

    public static String pass_d = "";


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_driver_login);

        btn0_d = (Button) findViewById(R.id.d_button0);
        btn1_d = (Button) findViewById(R.id.d_button1);
        btn2_d = (Button) findViewById(R.id.d_button2);
        btn3_d = (Button) findViewById(R.id.d_button3);
        btn4_d = (Button) findViewById(R.id.d_button4);
        btn5_d = (Button) findViewById(R.id.d_button5);
        btn6_d = (Button) findViewById(R.id.d_button6);
        btn7_d = (Button) findViewById(R.id.d_button7);
        btn8_d = (Button) findViewById(R.id.d_button8);
        btn9_d = (Button) findViewById(R.id.d_button9);
        btnC_d = (Button) findViewById(R.id.d_buttonC);
        driver_password = (EditText) findViewById(R.id.driver_password);
        driver_name = (TextView) findViewById(R.id.driver_name);
        driver_img = (ImageView) findViewById(R.id.driver_img);
        driver_login = (Button) findViewById(R.id.driver_login);

        driver_name.setText(getIntent().getStringExtra("name"));
        //hide keypad
        driver_password.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                v.onTouchEvent(event);
                InputMethodManager imm = (InputMethodManager)v.getContext().getSystemService(Context.INPUT_METHOD_SERVICE);
                if (imm != null) {
                    imm.hideSoftInputFromWindow(v.getWindowToken(), 0);
                }
                return true;
            }
        });

        btn0_d.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (driver_password.getText().length()<4) {
                    driver_password.setText(driver_password.getText() + "*");
                    pass_d = pass_d + "0";
                }
            }
        });
        btn1_d.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (driver_password.getText().length()<4) {
                    driver_password.setText(driver_password.getText() + "*");
                    pass_d = pass_d + "1";
                }
            }
        });
        btn2_d.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (driver_password.getText().length()<4) {
                    driver_password.setText(driver_password.getText() + "*");
                    pass_d = pass_d + "2";
                }
            }
        });
        btn3_d.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (driver_password.getText().length()<4) {
                    driver_password.setText(driver_password.getText() + "*");
                    pass_d = pass_d + "3";
                }
            }
        });
        btn4_d.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (driver_password.getText().length()<4) {
                    driver_password.setText(driver_password.getText() + "*");
                    pass_d = pass_d + "4";
                }
            }
        });
        btn5_d.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (driver_password.getText().length()<4) {
                    driver_password.setText(driver_password.getText() + "*");
                    pass_d = pass_d + "5";
                }
            }
        });
        btn6_d.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (driver_password.getText().length()<4) {
                    driver_password.setText(driver_password.getText() + "*");
                    pass_d = pass_d + "6";
                }
            }
        });
        btn7_d.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (driver_password.getText().length()<4) {
                    driver_password.setText(driver_password.getText() + "*");
                    pass_d = pass_d + "7";
                }
            }
        });
        btn8_d.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (driver_password.getText().length()<4) {
                    driver_password.setText(driver_password.getText() + "*");
                    pass_d = pass_d + "8";
                }
            }
        });
        btn9_d.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (driver_password.getText().length()<4) {
                    driver_password.setText(driver_password.getText() + "*");
                    pass_d = pass_d + "9";
                }
            }
        });
        btnC_d.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                driver_password.setText("");
                pass_d = "";
            }
        });

        driver_login.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (driver_password.getText().length()!=4){
                    Toast.makeText(getApplicationContext(),"Enter your 4digit Password",Toast.LENGTH_LONG).show();
                }
                else {
                    Toast.makeText(getApplicationContext(), "Login Sucessful password:"+pass_d, Toast.LENGTH_LONG).show();
                    if (pass_d.equals(getIntent().getStringExtra("pin"))){
                        Toast.makeText(getApplicationContext(), "Sucess", Toast.LENGTH_LONG).show();
                    }
                    pass_d = "";
                    driver_password.setText("");

                }
            }
        });




    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_driver_login, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
