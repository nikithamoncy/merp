package com.example.user.merp;

import android.app.ProgressDialog;
import android.graphics.Color;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.view.Display;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.AsyncHttpResponseHandler;
import com.loopj.android.http.RequestParams;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLEncoder;


public class ReturnByActivity extends ActionBarActivity {

    LinearLayout layout_rb;

    public static int width;
    public static int height;
    public static Display display;

    ProgressDialog prgDialog;

    EditText[] et = new EditText[100];
    EditText[] et2 = new EditText[100];
    TextView[] tv = new TextView[100];
    JSONArray jsonArray;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_return_by);

        prgDialog = new ProgressDialog(this);
        // Set Progress Dialog Text
        prgDialog.setMessage("Please wait...");
        // Set Cancelable as False
        prgDialog.setCancelable(false);

        layout_rb = (LinearLayout) findViewById(R.id.layout_rb);
        display = ((WindowManager) getSystemService(WINDOW_SERVICE)).getDefaultDisplay();
        width = display.getWidth();
        height = display.getHeight();

        RequestParams params = new RequestParams();
        invokeWS(params);


        LinearLayout hl = new LinearLayout(this);
        hl.setOrientation(LinearLayout.HORIZONTAL);
        hl.setPadding((width / 2) - 200, 50, 0, 25);
        layout_rb.addView(hl);

        TextView tv1 = new TextView(this);
        tv1.setPadding(170, 0, 0, 0);
        tv1.setTextColor(Color.BLACK);
        tv1.setTextSize(20);
        tv1.setText("Return");
        hl.addView(tv1);

        TextView tv2 = new TextView(this);
        tv2.setPadding(150, 0, 0, 0);
        tv2.setTextColor(Color.BLACK);
        tv2.setTextSize(20);
        tv2.setText("Leak");
        hl.addView(tv2);

    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_return_by, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    public void invokeWS(RequestParams params){
        // Show Progress Dialog
        prgDialog.show();
        // Make RESTful webservice call using AsyncHttpClient object
        AsyncHttpClient client = new AsyncHttpClient();
        client.get("http://merp-infofac1.rhcloud.com/Merp/dispatch/getWarehouseProducts/",params ,new AsyncHttpResponseHandler() {
            // When the response returned by REST has Http response code '200'
            @Override
            public void onSuccess(String response) {
                // Hide Progress Dialog
                prgDialog.hide();
                try {
                    // JSON Object
                    jsonArray = new JSONArray(response);
                    for (int i = 0;i < jsonArray.length();i++){
                        JSONObject jsonObject = jsonArray.getJSONObject(i);

                        LinearLayout ll = new LinearLayout(getApplicationContext());
                        ll.setLayoutParams(new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT,
                                LinearLayout.LayoutParams.WRAP_CONTENT));
                        ll.setOrientation(LinearLayout.HORIZONTAL);
                        ll.setPadding((width/2)-250, 10, 0, 0);
                        ll.setGravity(View.TEXT_ALIGNMENT_CENTER);
                        layout_rb.addView(ll);

                        tv[i] = new TextView(getApplicationContext());
                        tv[i].setText(jsonObject.getString("productName"));
                        tv[i].setTextColor(Color.BLACK);
                        tv[i].setTextSize(20);
                        tv[i].setPadding(0, 0, 50, 0);
                        tv[i].setWidth(200);
                        tv[i].setHeight(100);
                        ll.addView(tv[i]);

                        et[i] = new EditText(getApplicationContext());
                        et[i].setHintTextColor(Color.GRAY);
                        et[i].setTextColor(Color.BLACK);
                        et[i].setWidth(250);
                        et[i].setBackgroundResource(android.R.drawable.edit_text);
                        ll.addView(et[i]);

                        et2[i] = new EditText(getApplicationContext());
                        et2[i].setHintTextColor(Color.GRAY);
                        et2[i].setTextColor(Color.BLACK);
                        et2[i].setWidth(250);
                        et2[i].setBackgroundResource(android.R.drawable.edit_text);
                        ll.addView(et2[i]);

                    }


                    LinearLayout btn_ll = new LinearLayout(getApplicationContext());
                    btn_ll.setOrientation(LinearLayout.HORIZONTAL);
                    btn_ll.setPadding((width/2), 10, 0, 0);
                    layout_rb.addView(btn_ll);
                    Button b1 = new Button(getApplicationContext());
                    b1.setText("Submit");
                    btn_ll.addView(b1);
                    Button b2 = new Button(getApplicationContext());
                    b2.setText("Cancel");
                    btn_ll.addView(b2);

                    b1.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            for (int i = 0; i < jsonArray.length(); i++) {
                                try {
                                    JSONObject jsonObject = jsonArray.getJSONObject(i);
                                    Toast.makeText(getApplicationContext(),jsonObject.getString("productName")+": return-"
                                            + et[i].getText()+" leak-"+et2[i].getText(), Toast.LENGTH_LONG).show();

                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }
                            }
                        }
                    });

                    b2.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            //sendPostRequest();
                            for (int i = 0;i < jsonArray.length();i++) {
                                et[i].setText("");
                                et2[i].setText("");
                            }
                        }
                    });


                } catch (JSONException e) {
                    // TODO Auto-generated catch block
                    Toast.makeText(getApplicationContext(), "Error Occured [Server's JSON response might be invalid]!",
                            Toast.LENGTH_LONG).show();
                    Toast.makeText(getApplicationContext(),""+e,Toast.LENGTH_LONG).show();
                    e.printStackTrace();

                }
            }
            // When the response returned by REST has Http response code other than '200'
            @Override
            public void onFailure(int statusCode, Throwable error,
                                  String content) {
                // When Http response code is '404'
                if(statusCode == 404){
                    Toast.makeText(getApplicationContext(), "Requested resource not found", Toast.LENGTH_LONG).show();
                }
                // When Http response code is '500'
                else if(statusCode == 500){
                    Toast.makeText(getApplicationContext(), "Something went wrong at server end", Toast.LENGTH_LONG).show();
                }
                // When Http response code other than 404, 500
                else{
                    Toast.makeText(getApplicationContext(), "Unexpected Error occcured! [Most common Error: Device might not be " +
                            "connected to Internet or remote server is not up and running]", Toast.LENGTH_LONG).show();
                }
            }
        });
    }



    private void sendPostRequest() {

        class SendPostReqAsyncTask extends AsyncTask<String, Void, String> {

            @Override
            protected String doInBackground(String... params) {

                //prgDialog.show();


                StringBuilder sb = new StringBuilder();

                String http = "http://merp-infofac1.rhcloud.com/Merp/dispatch/stockUpdate";


                HttpURLConnection urlConnection=null;
                try {
                    URL url = new URL(http);
                    urlConnection = (HttpURLConnection) url.openConnection();
                    urlConnection.setDoOutput(true);
                    urlConnection.setRequestMethod("POST");
                    urlConnection.setUseCaches(false);
                    urlConnection.setConnectTimeout(10000);
                    urlConnection.setReadTimeout(10000);
                    urlConnection.setRequestProperty("Content-Type","application/json");
                    urlConnection.connect();

                    //Create JSONObject here
                    JSONObject jsonParam = new JSONObject();

                    jsonParam.put("userName", "Pooja");
                    jsonParam.put("productName", "Milk");
                    jsonParam.put("quantity", "5");
                    jsonParam.put("transactionType", "WA");
                    jsonParam.put("source", "Warehouse");
                    jsonParam.put("destination", "Shop");
                    jsonParam.put("warehouseName", "Pooja Milk");


                    OutputStreamWriter out = new   OutputStreamWriter(urlConnection.getOutputStream());
                    out.write(URLEncoder.encode(jsonParam.toString(), "UTF-8"));
                    out.flush();
                    out.close();

                    int HttpResult =urlConnection.getResponseCode();
                    if(HttpResult ==HttpURLConnection.HTTP_OK){
                        BufferedReader br = new BufferedReader(new InputStreamReader(
                                urlConnection.getInputStream(),"utf-8"));
                        String line = null;
                        while ((line = br.readLine()) != null) {
                            sb.append(line + "\n");
                        }
                        br.close();

                        Toast.makeText(getApplicationContext(),"result :"+sb.toString(),Toast.LENGTH_LONG).show();

                    }else{
                        System.out.println(urlConnection.getResponseMessage());
                    }
                } catch (MalformedURLException e) {

                    e.printStackTrace();
                }
                catch (IOException e) {

                    e.printStackTrace();
                } catch (JSONException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }finally{
                    if(urlConnection!=null)
                        urlConnection.disconnect();
                }

                //prgDialog.hide();
                return null;
            }

            @Override
            protected void onPostExecute(String result) {
                super.onPostExecute(result);

                    //Toast.makeText(getApplicationContext(), " "+result, Toast.LENGTH_LONG).show();

            }
        }

        SendPostReqAsyncTask sendPostReqAsyncTask = new SendPostReqAsyncTask();
        sendPostReqAsyncTask.execute();
    }

}
