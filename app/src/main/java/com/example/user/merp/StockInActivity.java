package com.example.user.merp;

import android.app.ProgressDialog;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.view.Display;
import android.view.Gravity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.JsonObject;
import com.koushikdutta.async.future.FutureCallback;
import com.koushikdutta.ion.Ion;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.AsyncHttpResponseHandler;
import com.loopj.android.http.RequestParams;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;


public class StockInActivity extends ActionBarActivity {

    LinearLayout layout_si;
    TableLayout table_layout_si;
    TableRow table_row_name_si;
    TableRow table_row_content_si;
    ProgressDialog prgDialog;

    public static int width;
    public static int height;
    public static Display display;

    EditText[] et = new EditText[100];
    TextView[] tv = new TextView[100];
    JSONArray jsonArray;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_stock_in);


        prgDialog = new ProgressDialog(this);
        // Set Progress Dialog Text
        prgDialog.setMessage("Please wait...");
        // Set Cancelable as False
        prgDialog.setCancelable(false);

        display = ((WindowManager) getSystemService(WINDOW_SERVICE)).getDefaultDisplay();
        width = display.getWidth();
        height = display.getHeight();

        layout_si = (LinearLayout) findViewById(R.id.layout_si);
        table_layout_si = (TableLayout) findViewById(R.id.table_layout_si);
        table_row_name_si = (TableRow) findViewById(R.id.table_row_name_si);
        table_row_content_si = (TableRow) findViewById(R.id.table_row_content_si);

        RequestParams params = new RequestParams();
        invokeWS(params);


       /* b1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                RequestParams params = new RequestParams();
                params.put("userName", "Pooja");
                params.put("productName", "Milk");
                params.put("quantity", "5");
                params.put("driverName", "0");
                params.put("routeName", "0");
                params.put("transactionType", "WA");
                params.put("source", "Warehouse");
                params.put("destination", "Shop");
                params.put("warehouseName", "Pooja Milk");
                invokeWS2(params);
            }
        });*/
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    public void invokeWS(RequestParams params){
        // Show Progress Dialog
        prgDialog.show();
        // Make RESTful webservice call using AsyncHttpClient object
        AsyncHttpClient client = new AsyncHttpClient();
        client.get("http://merp-infofac1.rhcloud.com/Merp/dispatch/getWarehouseProducts/",params ,new AsyncHttpResponseHandler() {
            // When the response returned by REST has Http response code '200'
            @Override
            public void onSuccess(String response) {
                // Hide Progress Dialog
                prgDialog.hide();
                try {
                    // JSON Object

                    jsonArray = new JSONArray(response);
                    for (int i = 0;i < jsonArray.length();i++){
                        JSONObject jsonObject = jsonArray.getJSONObject(i);

                        TextView item = new TextView(StockInActivity.this);
                        item.setWidth(width/2);
                        item.setGravity(Gravity.CENTER);
                        item.setBackgroundResource(android.R.drawable.edit_text);
                        item.setText(" "+jsonObject.getString("productName"));

                        TextView content = new TextView(StockInActivity.this);
                        content.setGravity(Gravity.CENTER);
                        content.setBackgroundResource(android.R.drawable.edit_text);
                        content.setText(""+jsonObject.getString("productCount"));

                        table_row_name_si.addView(item);
                        table_row_content_si.addView(content);

                        LinearLayout ll = new LinearLayout(getApplicationContext());
                        ll.setLayoutParams(new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT,
                                LinearLayout.LayoutParams.WRAP_CONTENT));
                        ll.setOrientation(LinearLayout.HORIZONTAL);
                        ll.setPadding((width/2)-200, 10, 0, 0);
                        ll.setGravity(View.TEXT_ALIGNMENT_CENTER);
                        layout_si.addView(ll);

                        tv[i] = new TextView(getApplicationContext());
                        tv[i].setText(jsonObject.getString("productName"));
                        tv[i].setTextColor(Color.BLACK);
                        tv[i].setWidth(100);
                        tv[i].setHeight(100);
                        ll.addView(tv[i]);

                        et[i] = new EditText(getApplicationContext());
                        et[i].setHintTextColor(Color.GRAY);
                        et[i].setTextColor(Color.BLACK);
                        et[i].setWidth(250);
                        et[i].setBackgroundResource(android.R.drawable.edit_text);
                        ll.addView(et[i]);
                    }

                    LinearLayout btn_ll = new LinearLayout(getApplicationContext());
                    btn_ll.setOrientation(LinearLayout.HORIZONTAL);
                    btn_ll.setPadding((width/2)-200, 10, 0, 0);
                    layout_si.addView(btn_ll);
                    Button b1 = new Button(getApplicationContext());
                    b1.setText("Submit");
                    btn_ll.addView(b1);
                    Button b2 = new Button(getApplicationContext());
                    b2.setText("Cancel");
                    btn_ll.addView(b2);

                    b1.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            for (int i = 0; i < jsonArray.length(); i++) {
                                try {
                                    JSONObject jsonObject = jsonArray.getJSONObject(i);
                                    Toast.makeText(getApplicationContext(), jsonObject.getString("productName")+ ":"
                                            + et[i].getText(), Toast.LENGTH_LONG).show();

                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }
                            }
                        }

                    });

                    b2.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            for (int i = 0;i < jsonArray.length();i++) {
                                et[i].setText("");
                            }

                           /* RequestParams params = new RequestParams();
                            params.put("userName", "Pooja");
                            params.put("productName", "Milk");
                            params.put("quantity", "5");
                            params.put("driverName", "0");
                            params.put("routeName", "0");
                            params.put("transactionType", "WA");
                            params.put("source", "Warehouse");
                            params.put("destination", "Shop");
                            params.put("warehouseName", "Pooja Milk");
                            invokeWS2(params);*/

                            JsonObject json = new JsonObject();
                            json.addProperty("userName", "Pooja");
                            json.addProperty("productName", "Milk");
                            json.addProperty("quantity", "5");
                            json.addProperty("driverName", "0");
                            json.addProperty("routeName", "0");
                            json.addProperty("transactionType", "WA");
                            json.addProperty("source", "Warehouse");
                            json.addProperty("destination", "Shop");
                            json.addProperty("warehouseName", "Pooja Milk");



                            Ion.with(getApplicationContext())
                                    .load("http://merp-infofac1.rhcloud.com/Merp/dispatch/stockUpdate")
                                    .setJsonObjectBody(json)
                                    .asJsonObject()
                                    .setCallback(new FutureCallback<JsonObject>() {
                                        @Override
                                        public void onCompleted(Exception e, JsonObject result) {
                                            // do stuff with the result or error
                                            Toast.makeText(getApplicationContext(),""+result,Toast.LENGTH_LONG).show();
                                        }
                                    });

                        }
                    });



                } catch (JSONException e) {
                    // TODO Auto-generated catch block
                    Toast.makeText(getApplicationContext(), "Error Occured [Server's JSON response might be invalid]!",
                            Toast.LENGTH_LONG).show();
                    Toast.makeText(getApplicationContext(),""+e,Toast.LENGTH_LONG).show();
                    e.printStackTrace();

                }
            }
            // When the response returned by REST has Http response code other than '200'
            @Override
            public void onFailure(int statusCode, Throwable error,
                                  String content) {
                // When Http response code is '404'
                if(statusCode == 404){
                    Toast.makeText(getApplicationContext(), "Requested resource not found", Toast.LENGTH_LONG).show();
                }
                // When Http response code is '500'
                else if(statusCode == 500){
                    Toast.makeText(getApplicationContext(), "Something went wrong at server end", Toast.LENGTH_LONG).show();
                }
                // When Http response code other than 404, 500
                else{
                    Toast.makeText(getApplicationContext(), "Unexpected Error occcured! [Most common Error: Device might not be " +
                            "connected to Internet or remote server is not up and running]", Toast.LENGTH_LONG).show();
                }
            }
        });
    }


    public void invokeWS2(RequestParams params){
        // Show Progress Dialog
        prgDialog.show();


        prgDialog.hide();

/*
        // Make RESTful webservice call using AsyncHttpClient object
        AsyncHttpClient client = new AsyncHttpClient();
        client.addHeader("Content-Type", "application/json");
        client.post("http://merp-infofac1.rhcloud.com/Merp/dispatch/stockUpdate",params ,new AsyncHttpResponseHandler() {
            // When the response returned by REST has Http response code '200'
            @Override
            public void onSuccess(String response) {
                // Hide Progress Dialog
                prgDialog.hide();
                try {
                    // JSON Object
                    JSONObject obj = new JSONObject(response);
                    Toast.makeText(getApplicationContext(), "You are successfully registered!"+obj.getString("message"), Toast.LENGTH_LONG).show();

                } catch (JSONException e) {
                    // TODO Auto-generated catch block
                    Toast.makeText(getApplicationContext(), "Error Occured [Server's JSON response might be invalid]!", Toast.LENGTH_LONG).show();
                    e.printStackTrace();

                }
            }
            // When the response returned by REST has Http response code other than '200'
            @Override
            public void onFailure(int statusCode, Throwable error,
                                  String content) {
                // Hide Progress Dialog
                prgDialog.hide();
                // When Http response code is '404'
                if(statusCode == 404){
                    Toast.makeText(getApplicationContext(), "Requested resource not found", Toast.LENGTH_LONG).show();
                }
                // When Http response code is '500'
                else if(statusCode == 500){
                    Toast.makeText(getApplicationContext(), "Something went wrong at server end", Toast.LENGTH_LONG).show();
                }
                // When Http response code other than 404, 500
                else{
                    Toast.makeText(getApplicationContext(), "Unexpected Error occcured! [Most common Error: Device might not be connected to Internet or remote server is not up and running]", Toast.LENGTH_LONG).show();
                }
            }
        });*/
    }
}
