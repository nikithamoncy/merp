package com.example.user.merp;

import android.app.ProgressDialog;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.view.Display;
import android.view.Gravity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;
import android.widget.Toast;

import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.AsyncHttpResponseHandler;
import com.loopj.android.http.RequestParams;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;


public class StockOutActivity extends ActionBarActivity {

    ProgressDialog prgDialog;

    LinearLayout layout_so;
    TableLayout table_layout_so;
    TableRow table_row_name_so;
    TableRow table_row_content_so;

    public static int width;
    public static int height;
    public static Display display;

    EditText[] et = new EditText[100];
    TextView[] tv = new TextView[100];
    JSONArray jsonArray;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_stock_out);

        prgDialog = new ProgressDialog(this);
        // Set Progress Dialog Text
        prgDialog.setMessage("Please wait...");
        // Set Cancelable as False
        prgDialog.setCancelable(false);

        display = ((WindowManager) getSystemService(WINDOW_SERVICE)).getDefaultDisplay();
        width = display.getWidth();
        height = display.getHeight();

        layout_so = (LinearLayout) findViewById(R.id.layout_so);
        table_layout_so = (TableLayout) findViewById(R.id.table_layout_so);
        table_row_name_so = (TableRow) findViewById(R.id.table_row_name_so);
        table_row_content_so = (TableRow) findViewById(R.id.table_row_content_so);

        RequestParams params = new RequestParams();
        invokeWS(params);

    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_stock_out, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }


    public void invokeWS(RequestParams params){
        // Show Progress Dialog
        prgDialog.show();
        // Make RESTful webservice call using AsyncHttpClient object
        AsyncHttpClient client = new AsyncHttpClient();
        client.get("http://merp-infofac1.rhcloud.com/Merp/dispatch/getWarehouseProducts/",params ,new AsyncHttpResponseHandler() {
            // When the response returned by REST has Http response code '200'
            @Override
            public void onSuccess(String response) {
                // Hide Progress Dialog
                prgDialog.hide();
                try {
                    // JSON Object
                    jsonArray = new JSONArray(response);
                    for (int i = 0;i < jsonArray.length();i++){
                        JSONObject jsonObject = jsonArray.getJSONObject(i);

                        TextView item = new TextView(StockOutActivity.this);
                        item.setWidth(width/2);
                        item.setGravity(Gravity.CENTER);
                        item.setBackgroundResource(android.R.drawable.edit_text);
                        item.setText(jsonObject.getString("productName"));

                        TextView content = new TextView(StockOutActivity.this);
                        content.setGravity(Gravity.CENTER);
                        content.setBackgroundResource(android.R.drawable.edit_text);
                        content.setText(jsonObject.getString("productCount"));

                        table_row_name_so.addView(item);
                        table_row_content_so.addView(content);

                        LinearLayout ll = new LinearLayout(getApplicationContext());
                        ll.setLayoutParams(new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT,
                                LinearLayout.LayoutParams.WRAP_CONTENT));
                        ll.setOrientation(LinearLayout.HORIZONTAL);
                        ll.setPadding((width/2)-200, 10, 0, 0);
                        ll.setGravity(View.TEXT_ALIGNMENT_CENTER);
                        layout_so.addView(ll);

                        tv[i] = new TextView(getApplicationContext());
                        tv[i].setText(jsonObject.getString("productName"));
                        tv[i].setTextColor(Color.BLACK);
                        tv[i].setWidth(100);
                        tv[i].setHeight(100);
                        ll.addView(tv[i]);

                        et[i] = new EditText(getApplicationContext());
                        et[i].setHintTextColor(Color.GRAY);
                        et[i].setTextColor(Color.BLACK);
                        et[i].setWidth(250);
                        et[i].setBackgroundResource(android.R.drawable.edit_text);
                        ll.addView(et[i]);
                    }

                    LinearLayout dvr_ll = new LinearLayout(getApplicationContext());
                    dvr_ll.setLayoutParams(new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT,
                            LinearLayout.LayoutParams.WRAP_CONTENT));
                    dvr_ll.setOrientation(LinearLayout.HORIZONTAL);
                    dvr_ll.setPadding((width/2)-200, 10, 0, 0);
                    dvr_ll.setGravity(View.TEXT_ALIGNMENT_CENTER);
                    layout_so.addView(dvr_ll);

                    final TextView tvd = new TextView(getApplicationContext());
                    tvd.setText("Driver");
                    tvd.setTextColor(Color.BLACK);
                    tvd.setWidth(100);
                    tvd.setHeight(100);
                    dvr_ll.addView(tvd);

                    final EditText etd = new EditText(getApplicationContext());
                    etd.setHintTextColor(Color.GRAY);
                    etd.setTextColor(Color.BLACK);
                    etd.setWidth(250);
                    etd.setBackgroundResource(android.R.drawable.edit_text);
                    dvr_ll.addView(etd);

                    LinearLayout rt_ll = new LinearLayout(getApplicationContext());
                    rt_ll.setLayoutParams(new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT,
                            LinearLayout.LayoutParams.WRAP_CONTENT));
                    rt_ll.setOrientation(LinearLayout.HORIZONTAL);
                    rt_ll.setPadding((width/2)-200, 10, 0, 0);
                    rt_ll.setGravity(View.TEXT_ALIGNMENT_CENTER);
                    layout_so.addView(rt_ll);

                    final TextView tvr = new TextView(getApplicationContext());
                    tvr.setText("Route");
                    tvr.setTextColor(Color.BLACK);
                    tvr.setWidth(100);
                    tvr.setHeight(100);
                    rt_ll.addView(tvr);

                    final EditText etr = new EditText(getApplicationContext());
                    etr.setHintTextColor(Color.GRAY);
                    etr.setTextColor(Color.BLACK);
                    etr.setWidth(250);
                    etr.setBackgroundResource(android.R.drawable.edit_text);
                    rt_ll.addView(etr);

                    LinearLayout btn_ll = new LinearLayout(getApplicationContext());
                    btn_ll.setOrientation(LinearLayout.HORIZONTAL);
                    btn_ll.setPadding((width/2)-200, 10, 0, 0);
                    layout_so.addView(btn_ll);
                    Button b1 = new Button(getApplicationContext());
                    b1.setText("Submit");
                    btn_ll.addView(b1);
                    Button b2 = new Button(getApplicationContext());
                    b2.setText("Cancel");
                    btn_ll.addView(b2);

                    b1.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {

                            for (int i = 0; i < jsonArray.length(); i++) {
                                try {
                                    JSONObject jsonObject = jsonArray.getJSONObject(i);
                                    Toast.makeText(getApplicationContext(),jsonObject.getString("productName")+":"+ et[i].getText(),
                                            Toast.LENGTH_LONG).show();

                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }
                            }
                            Toast.makeText(getApplicationContext(),tvd.getText()+":"+etd.getText(), Toast.LENGTH_LONG).show();
                            Toast.makeText(getApplicationContext(),tvr.getText()+":"+etr.getText(), Toast.LENGTH_LONG).show();



                        }
                    });

                    b2.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            for (int i = 0;i < jsonArray.length();i++){
                                et[i].setText("");
                            }
                            etd.setText("");
                            etr.setText("");
                        }
                    });


                } catch (JSONException e) {
                    // TODO Auto-generated catch block
                    Toast.makeText(getApplicationContext(), "Error Occured [Server's JSON response might be invalid]!",
                            Toast.LENGTH_LONG).show();
                    Toast.makeText(getApplicationContext(),""+e,Toast.LENGTH_LONG).show();
                    e.printStackTrace();

                }
            }
            // When the response returned by REST has Http response code other than '200'
            @Override
            public void onFailure(int statusCode, Throwable error,
                                  String content) {
                // When Http response code is '404'
                if(statusCode == 404){
                    Toast.makeText(getApplicationContext(), "Requested resource not found", Toast.LENGTH_LONG).show();
                }
                // When Http response code is '500'
                else if(statusCode == 500){
                    Toast.makeText(getApplicationContext(), "Something went wrong at server end", Toast.LENGTH_LONG).show();
                }
                // When Http response code other than 404, 500
                else{
                    Toast.makeText(getApplicationContext(), "Unexpected Error occcured! [Most common Error: Device might not be " +
                            "connected to Internet or remote server is not up and running]", Toast.LENGTH_LONG).show();
                }
            }
        });
    }


}
