package com.example.user.merp;

import android.content.Context;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;


public class WareHouseLoginActivity extends ActionBarActivity {

    Button btn0_wh;
    Button btn1_wh;
    Button btn2_wh;
    Button btn3_wh;
    Button btn4_wh;
    Button btn5_wh;
    Button btn6_wh;
    Button btn7_wh;
    Button btn8_wh;
    Button btn9_wh;
    Button btnC_wh;

    EditText wh_password;
    TextView wh_name;
    ImageView wh_img;
    Button wh_login;

    public static String pass_wh = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_ware_house_login);

        btn0_wh = (Button) findViewById(R.id.wh_button0);
        btn1_wh = (Button) findViewById(R.id.wh_button1);
        btn2_wh = (Button) findViewById(R.id.wh_button2);
        btn3_wh = (Button) findViewById(R.id.wh_button3);
        btn4_wh = (Button) findViewById(R.id.wh_button4);
        btn5_wh = (Button) findViewById(R.id.wh_button5);
        btn6_wh = (Button) findViewById(R.id.wh_button6);
        btn7_wh = (Button) findViewById(R.id.wh_button7);
        btn8_wh = (Button) findViewById(R.id.wh_button8);
        btn9_wh = (Button) findViewById(R.id.wh_button9);
        btnC_wh = (Button) findViewById(R.id.wh_buttonC);
        wh_password = (EditText) findViewById(R.id.wh_password);
        wh_name = (TextView) findViewById(R.id.wh_name);
        wh_img = (ImageView) findViewById(R.id.wh_img);
        wh_login = (Button) findViewById(R.id.wh_login);

        //hide keypad
        wh_password.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                v.onTouchEvent(event);
                InputMethodManager imm = (InputMethodManager)v.getContext().getSystemService(Context.INPUT_METHOD_SERVICE);
                if (imm != null) {
                    imm.hideSoftInputFromWindow(v.getWindowToken(), 0);
                }
                return true;
            }
        });

        btn0_wh.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (wh_password.getText().length()<4) {
                    pass_wh = pass_wh + "0";
                    wh_password.setText(wh_password.getText()+"*");
                }
            }
        });
        btn1_wh.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (wh_password.getText().length()<4) {
                    pass_wh = pass_wh + "1";
                    wh_password.setText(wh_password.getText()+"*");
                }
            }
        });
        btn2_wh.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (wh_password.getText().length()<4) {
                    pass_wh = pass_wh + "2";
                    wh_password.setText(wh_password.getText()+"*");
                }
            }
        });
        btn3_wh.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (wh_password.getText().length()<4) {
                    pass_wh = pass_wh + "3";
                    wh_password.setText(wh_password.getText()+"*");
                }
            }
        });
        btn4_wh.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (wh_password.getText().length()<4) {
                    pass_wh = pass_wh + "4";
                    wh_password.setText(wh_password.getText()+"*");
                }
            }
        });
        btn5_wh.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (wh_password.getText().length()<4) {
                    pass_wh = pass_wh + "5";
                    wh_password.setText(wh_password.getText()+"*");
                }
            }
        });
        btn6_wh.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (wh_password.getText().length()<4) {
                    pass_wh = pass_wh + "6";
                    wh_password.setText(wh_password.getText()+"*");
                }
            }
        });
        btn7_wh.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (wh_password.getText().length()<4) {
                    pass_wh = pass_wh + "7";
                    wh_password.setText(wh_password.getText()+"*");
                }
            }
        });
        btn8_wh.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (wh_password.getText().length()<4) {
                    pass_wh = pass_wh + "8";
                    wh_password.setText(wh_password.getText()+"*");
                }
            }
        });
        btn9_wh.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (wh_password.getText().length()<4) {
                    pass_wh = pass_wh + "9";
                    wh_password.setText(wh_password.getText()+"*");
                }
            }
        });
        btnC_wh.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                wh_password.setText("");
                pass_wh = "";
            }
        });
        wh_login.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (wh_password.getText().length()!=4){
                    Toast.makeText(getApplicationContext(),"Enter your 4digit Password",Toast.LENGTH_LONG).show();
                }
                else {
                    Toast.makeText(getApplicationContext(), "Login Sucessful password:"+pass_wh, Toast.LENGTH_LONG).show();
                    pass_wh = "";
                    wh_password.setText("");
                }
            }
        });
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_ware_house_login, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
